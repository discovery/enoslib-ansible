#!/usr/bin/python -tt
# GNU General Public License v3.0+ (see COPYING or https://www.gnu.org/licenses/gpl-3.0.txt)
# SPDX-License-Identifier: GPL-3.0-or-later
# SPDX-FileCopyrightText: Ansible Project, 2020

from setuptools import setup


__version__ = '42.42.42.42'
__author__ = 'EnOSlib team'


with open('README.rst', 'r') as f:
    long_desc = f.read()

setup(
    name='enoslib-ansible',
    version=__version__,
    description='Radically simple IT automation -- For EnOSlib !',
    long_description=long_desc,
    author=__author__,
    author_email='enoslib.contact@inria.fr',
    url='https://discovery.gitlabpages.inria.fr/enoslib/',
    license='GPLv3+',
    packages=['ansible_collections'],
    exclude_package_data={
        'ansible_collections': [
            'community/general/.*',
            'community/general/docs/*',
            'community/general/tests/*',
            'community/docker/.*',
            'community/docker/docs/*',
            'community/docker/tests/*',
            'community/libvirt/.*',
            'community/libvirt/docs/*',
            'community/libvirt/tests/*',
            'ansible/posix/.*',
            'ansible/posix/docs/*',
            'ansible/posix/tests/*',
            'ansible/utils/.*',
            'ansible/utils/docs/*',
            'ansible/utils/tests/*',
         ],
    },
    include_package_data=True,
    install_requires=[
        # we relax a bit the ansible-core version
        # in comparison to what ansible is doing
        'ansible-core~=2.17.0',
    ],
    classifiers=[
        'Development Status :: 5 - Production/Stable',
        'Environment :: Console',
        'Framework :: Ansible',
        'Intended Audience :: Developers',
        'Intended Audience :: Information Technology',
        'Intended Audience :: System Administrators',
        'License :: OSI Approved :: GNU General Public License v3 or later (GPLv3+)',
        'Natural Language :: English',
        'Operating System :: POSIX',
        'Topic :: System :: Installation/Setup',
        'Topic :: System :: Systems Administration',
        'Topic :: Utilities',
    ],
    data_files=[],
    # Installing as zip files would break due to references to __file__
    zip_safe=False
)
